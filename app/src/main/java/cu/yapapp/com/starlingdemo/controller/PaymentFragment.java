package cu.yapapp.com.starlingdemo.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.MyAccountInformationManager;
import cu.yapapp.com.starlingdemo.utils.SharedPreferenceHelper;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.utils.UserInfo;
import cu.yapapp.com.starlingdemo.view.views.PaymentView;
import pay.eu.android.http.pg.WebControllerHelper;
import pay.eu.android.model.AccountModel;

/**
 * Created by gaganpreet on 14/9/17.
 */

public class PaymentFragment extends FragmentAppbar {

    private PaymentView paymentView;
    private MyAccountInformationManager myAccountInformationManager;
    private StarlingWebController starlingWebController;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (paymentView != null) return paymentView;
        paymentView = new PaymentView(getContext(), paymentViewCallbacks);
        starlingWebController = new StarlingWebController(getActivity());
        myAccountInformationManager = new MyAccountInformationManager(starlingWebController, myAccountInformationManagerListener);
        myAccountInformationManager.requestAccountDetail();
        return paymentView;
    }


    @Override
    public void onResume() {
        super.onResume();
        removeLeftRight();
        setTitleTxt(getString(R.string.app_name));

    }

    PaymentView.PaymentViewCallbacks paymentViewCallbacks = new PaymentView.PaymentViewCallbacks() {
        @Override
        public void onMyProfileClick() {
            replaceFragment(new MyBarcodeFragment(), true, true);
        }

        @Override
        public void onBankAccountClick() {
            replaceFragment(new FragmentAccountDetails(), true, true);
        }

        @Override
        public void onBeneficiariesClick() {
            replaceFragment(new FragmentPayees(), true, true);
        }


        @Override
        public void onTransactionsClick() {
            replaceFragment(new FragmentTransactions(), true, true);
        }

        @Override
        public void onLogoutClick() {
            WebControllerHelper.saveAccessTokenPref(getContext(), "");
            SharedPreferenceHelper.clearSharedPrfrence(getContext());

            popSingleFragment();

            replaceFragment(new LoginFragment(), true, true);
        }
    };


    MyAccountInformationManager.MyAccountInformationManagerListener myAccountInformationManagerListener = new MyAccountInformationManager.MyAccountInformationManagerListener() {
        @Override
        public void onAccountDetailsReceived(AccountModel accountModel) {
            UserInfo.getInstance().setAccountModel(accountModel);
        }

        @Override
        public void onError(String data) {
//            no code required for now as sdk needs to be updated.
        }
    };
}
