package cu.yapapp.com.starlingdemo.controller;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Field;

import cu.yapapp.com.starlingdemo.R;

/**
 * Created by yapapp on 24/11/16.
 */
public class YABaseActivity extends AppCompatActivity {
    public CoordinatorLayout coordinatorLayout;
    public RelativeLayout rlGuideLine;
    // public AppBarLayout appBarLayout;
    public FrameLayout container;
    public RelativeLayout containerToolbar;
    public LinearLayout llRightContainer, llLeftContainer;
    // public TabLayout tablayout;
    public Toolbar toolBar, searchtollbar;
    //public DrawerLayout mDrawerLayout;
    // public NavigationView navigationView;
    public ActionBar supportActionBar;
    public TextView title, tvSubTitle;
    //public ImageView imgBtnLeft, imgBtnRight,imgBtnRightSecond;
    public RelativeLayout relRightIcon;
    public android.widget.SearchView svSearchHolder;
    public ImageView mImg_search;
    public Menu mMenuSearch;
    public MenuItem mMenuItem_Search;
    public SearchView mSearchView;
    public EditText mEditTextSearchView;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


/*
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("isAppBar",((FragmentBase)getCurrentFragment()).showAppBar());
        outState.putBoolean("isDrawer",((FragmentBase)getCurrentFragment()).showDrawer());

    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        showHideAppBar(savedInstanceState.getBoolean("isAppBar"));
        enableDisableDrawer(savedInstanceState.getBoolean("isDrawer"));
    }*/
    @Override
    protected void onResume() {
        super.onResume();
      /* getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                +WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                +WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ya_base);
        initializeView();
    }


    public android.widget.SearchView getSearchView() {

        if (svSearchHolder == null) {
            svSearchHolder = (android.widget.SearchView) toolBar.findViewById(R.id.svSearchHolder);
        }
        return svSearchHolder;
    }

    public void initializeView() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        // appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        container = (FrameLayout) findViewById(R.id.container);
        containerToolbar = (RelativeLayout) findViewById(R.id.containerToolbar);
        rlGuideLine = (RelativeLayout) findViewById(R.id.rlGuideline);
        mImg_search = (ImageView) findViewById(R.id.img_search);
        //tablayout = (TabLayout) findViewById(R.id.tabs);
        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        // navigationView = (NavigationView) findViewById(R.id.nav_view);
        // mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer); // Adding menu icon to Toolbar
        supportActionBar = getSupportActionBar();
        setTitle("");
        //   imgBtnLeft = (ImageView) toolBar.findViewById(R.id.img_btn_left);
       /* imgBtnRight = (ImageView) toolBar.findViewById(R.id.img_btn_right);
        imgBtnRightSecond=(ImageView)toolBar.findViewById(R.id.img_btn_right_second);*/
        llRightContainer = (LinearLayout) toolBar.findViewById(R.id.llRightContainer);
        llLeftContainer = (LinearLayout) toolBar.findViewById(R.id.llLeftContainer);
        //relRightIcon = (RelativeLayout) toolBar.findViewById(R.id.relRightIcon);
        title = (TextView) toolBar.findViewById(R.id.title_tv);
        tvSubTitle = (TextView) toolBar.findViewById(R.id.tvSubTitle);

        /*if (supportActionBar != null) {
            VectorDrawableCompat indicator
                    = VectorDrawableCompat.create(getResources(), R.drawable.ic_menu, getTheme());
            indicator.setTint(ResourcesCompat.getColor(getResources(), R.color.white, getTheme()));
            supportActionBar.setHomeAsUpIndicator(indicator);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }*/

    /*    relRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onRightIconTapped();

            }
        });*/

       /* imgBtnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onLeftIconTapped();

            }
        });*/
        setSearchtollbar();
        mImg_search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mEditTextSearchView != null)
                {
                    mEditTextSearchView.requestFocus();
                    InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm1.showSoftInput(mEditTextSearchView, InputMethodManager.SHOW_IMPLICIT);
                }
                //mEditTextSearchView.setSelectAllOnFocus(true);
                mImg_search.setTag(1);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(R.id.searchtoolbar, 1, true, true);
                else
                    searchtollbar.setVisibility(View.VISIBLE);
                mMenuItem_Search.expandActionView();

            }
        });

    }

    public Toolbar getToolBar() {
        return toolBar;
    }

    public void showAlert(String message) {

        try {
            Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();

        } catch (Exception e) {

        }
    }

    public void addFragment(Fragment fragment, boolean addToBackStack) {
        inflateFragment(fragment, addToBackStack, TransactionType.ADD, true);
    }

    public void addFragment(Fragment fragment, boolean addToBackStack, boolean showAnimation) {
        inflateFragment(fragment, addToBackStack, TransactionType.ADD, showAnimation);
    }

    public ViewGroup getGuideLineRl() {
        return rlGuideLine;
    }

    public void inflateFragment(Fragment fragment
            , String tag, boolean addToBackStack, TransactionType transactionType, boolean showAnimation) {

        // Chat tag - com.yapapp.cu.controller.ChatFragment
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(tag);
        } else {
            tag = null;
        }


//        if (showAnimation)
//            transaction.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_from_top, R.anim.enter_from_top, R.anim.exit_from_bottom);


        switch (transactionType) {
            case REPLACE:
                transaction.replace(R.id.container, fragment, tag);
                break;

            case ADD:
                transaction.add(R.id.container, fragment, tag);
                break;
        }


        transaction.commitAllowingStateLoss();
    }

    public void inflateFragment(Fragment fragment, boolean addToBackStack, TransactionType transactionType, boolean showAnimation) {
        String tag = fragment.getClass().getCanonicalName();
        inflateFragment(fragment
                , tag, addToBackStack, transactionType, showAnimation);
    }


    public enum TransactionType {
        REPLACE, ADD
    }


    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        inflateFragment(fragment, addToBackStack, TransactionType.REPLACE, true);
    }


    public void replaceFragment(Fragment fragment, boolean addToBackStack, boolean showAnimation) {
        inflateFragment(fragment, addToBackStack, TransactionType.REPLACE, showAnimation);
    }

    public void replaceFragment(Fragment fragment, String tag, boolean addToBackStack, boolean showAnimation) {
        inflateFragment(fragment, tag, addToBackStack, TransactionType.REPLACE, showAnimation);
    }


    public int getFragmentCount() {
        return getSupportFragmentManager().getBackStackEntryCount();
    }

    public void showHideAppBar(boolean bool) {

        if (!getSupportActionBar().isShowing()) {
            getSupportActionBar().show();
        }

        containerToolbar.setVisibility(bool ? View.VISIBLE : View.GONE);
    }



    public void showHideSearch(boolean bool) {
        mImg_search.setVisibility(bool ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        //
        if (getFragmentCount() == 1) {
            finish();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //     getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void enableDisableDrawer(boolean bool) {

     /*   if (!bool) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }*/
    }


    public void popSingle()
    {
        try
        {
            FragmentManager fm = getSupportFragmentManager();
            fm.popBackStack();
        }
        catch (Exception w)
        {

        }
    }
    public void pop()
    {


            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                try {
                fm.popBackStack();
                }catch (IllegalStateException e){

                }
            }

    }
    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }
    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement/*
        if (id == android.R.id.home) {
            // mDrawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }
    public void onLeftIconTapped() {}
    public void onRightIconTapped() {}
    public void setSearchtollbar()
    {
        searchtollbar = (Toolbar) findViewById(R.id.searchtoolbar);
        if (searchtollbar != null)
        {
            searchtollbar.inflateMenu(R.menu.menu_search);
            mMenuSearch = searchtollbar.getMenu();
            searchtollbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    closeSearchView();
                }
            });
            mMenuItem_Search = mMenuSearch.findItem(R.id.action_filter_search);
            MenuItemCompat.setOnActionExpandListener(mMenuItem_Search, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item)
                {
                    closeSearchView();
                    return true;
                }
                @Override
                public boolean onMenuItemActionExpand(MenuItem item)
                {
                    return true;
                }
            });
            //initSearchView();
        }
    }




    public void closeSearchView()
    {
        mImg_search.setTag(0);
        if (mEditTextSearchView != null)
        {
            mEditTextSearchView.setText("");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            circleReveal(R.id.searchtoolbar, 1, true, false);
        else
            searchtollbar.setVisibility(View.GONE);
    }
    public void initSearchView(final SearchMenuInterface searchMenuInterface)
    {
        mSearchView = (SearchView) mMenuSearch.findItem(R.id.action_filter_search).getActionView();
        // Enable/Disable Submit button in the keyboard
        //mSearchView.setSubmitButtonEnabled(false);
        // Change search close button image
        ImageView closeButton = (ImageView) mSearchView.findViewById(R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.newsearch_close_red);
        // set hint and the text colors
        mEditTextSearchView = ((EditText) mSearchView.findViewById(R.id.search_src_text));
        mEditTextSearchView.setHint(getResources().getString(R.string.search_hint));
        mEditTextSearchView.setHintTextColor(getResources().getColor(R.color.search_hint_text));
        mEditTextSearchView.setTextColor(getResources().getColor(R.color.search_text));
        // set the cursor
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) mSearchView.findViewById(R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.search_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                searchMenuInterface.onQueryTextSubmit(query);
                mSearchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText)
            {
                searchMenuInterface.onQueryTextChange(newText);
                return true;
            }
            public void callSearch(String query)
            {
                Log.i("query", "" + query);
            }

        });
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow, final boolean isShow)
    {
        final View myView = findViewById(viewID);
        int width = myView.getWidth();
        if (posFromRight > 0)
            width -= (posFromRight * getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)) - (getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) / 2);
        if (containsOverflow)
            width -= getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);
        int cx = width;
        int cy = myView.getHeight() / 2;
        Animator anim;
        if (isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, (float) width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float) width, 0);
        anim.setDuration((long) 280);
        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isShow) {
                    super.onAnimationEnd(animation);

                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });
        // make the view visible and start the animation
        if (isShow)
        {
            myView.setVisibility(View.VISIBLE);
        }
        // start the animation
        anim.start();
    }
    public interface SearchMenuInterface
    {
        boolean onQueryTextSubmit(String query);
        boolean onQueryTextChange(String newText);
    }
}
