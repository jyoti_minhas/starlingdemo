package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;
import android.widget.TextView;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.BalanceModel;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class AccountDetailsView extends SuperView {

    private TextView tvClearedBalance;
    private TextView tvEffectiveBalance;
    private TextView tvPendingTransactions;
    private TextView tvAvailableToSpend;
    private TextView tvCurrency;
    private TextView tvAmount;
    private TextView tvAcc;


    public AccountDetailsView(Context context) {
        super(context);
        initValiables();
    }


    @Override
    public void initValiables() {

        inflate(getContext(), R.layout.fragment_account_balance, this);
        tvClearedBalance = (TextView) findViewById(R.id.tvClearedBalanceValue);
        tvEffectiveBalance = (TextView) findViewById(R.id.tvEffectiveBalanceValue);
        tvPendingTransactions = (TextView) findViewById(R.id.tvPendingTransactionsValue);
        tvAvailableToSpend = (TextView) findViewById(R.id.tvAvailableToSpendValue);
        tvCurrency = (TextView) findViewById(R.id.tvCurrencyValue);
        tvAmount = (TextView) findViewById(R.id.tvAmountValue);
        tvAcc = (TextView) findViewById(R.id.tvAccVal);


    }

    @Override
    public void setEventCallbacks() {
        /**
         * No code for now as Starling sdk needs to be updated.
         */
    }


    /**
     * In this method we show Account Details of user to
     *
     * @param object {@link AccountModel} instance.
     */
    public void showAccountDetails(AccountModel object) {
        String account = object.getAccountNumber();
        char lastFirst = account.charAt(account.length() - 2);
        char last = account.charAt(account.length() - 1);
        tvAcc.setText("XXXXXX" + lastFirst + last);
    }


    /**
     * In this method we show Account Details of user to
     *
     * @param object {@link BalanceModel} instance.
     */
    public void showBalance(BalanceModel object) {

        final String currency = " " + object.getCurrency();

        Utils utils = Utils.getInstance();

        tvAmount.setText(utils.parseDouble(object.getAmount()) + currency);
        tvAvailableToSpend.setText(utils.parseDouble(object.getAvailableBalanceToSpend()) + currency);
        tvClearedBalance.setText(utils.parseDouble(object.getClearBalance()) + currency);
        tvCurrency.setText(object.getCurrency());
        tvEffectiveBalance.setText(utils.parseDouble(object.getEffectiveBalance()) + currency);
        tvPendingTransactions.setText(utils.parseDouble(object.getPendingTransactionBalance()) + currency);
    }

}
