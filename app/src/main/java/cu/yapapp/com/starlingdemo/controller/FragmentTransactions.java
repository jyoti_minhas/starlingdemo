package cu.yapapp.com.starlingdemo.controller;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.TransactionsManager;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.view.views.TransactionsView;
import pay.eu.android.model.TransactionHistoryModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTransactions extends FragmentAppbar {


    private TransactionsView transactionsView;
    private TransactionsManager transactionsManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (transactionsView != null) return transactionsView;

        transactionsView = new TransactionsView(getContext());

        transactionsManager = new TransactionsManager(new StarlingWebController(getActivity()), managerCallbacks);
        transactionsManager.requestTransactions();
        return transactionsView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleTxt(getString(R.string.transactions));
    }

    TransactionsManager.TransactionManagerCallbacks managerCallbacks = new TransactionsManager.TransactionManagerCallbacks() {
        @Override
        public void onTransactionsReceived(TransactionHistoryModel payeeModel, String body) {
            transactionsView.loadTransactionHistory(payeeModel);
        }

        @Override
        public void onError(String body) {
            /**
             * No code for now as Starling  sdk needs to be updated.
             */
        }
    };


}
