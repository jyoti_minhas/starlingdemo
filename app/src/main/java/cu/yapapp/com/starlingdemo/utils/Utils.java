package cu.yapapp.com.starlingdemo.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;

import org.apache.commons.lang3.text.WordUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.dataModel.ModelContactShare;
import pay.eu.android.http.pg.WebControllerHelper;
import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AccountModel;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class Utils {

    private static Utils utils = new Utils();

    private static final String EMPTY = "";
    private final String TOKEN_DATE_TIME = ":";
    private final String TOKEN_SPACE = " ";

    private Utils() {
    }

    /**
     * It is static getter for Utils.
     *
     * @return Utils
     */
    public static Utils getInstance() {
        return utils;
    }


    /**
     * @param context Context
     * @return AccessTokenModel if it is saved in {@link android.content.SharedPreferences} else returns null;
     */
    public AccessTokenModel getAccessTokenModel(Context context) {
        String key = context.getString(R.string.sharedPredKey_LoginInfo);
        Gson gson = new Gson();
        String data = SharedPreferenceHelper.getSharedPreferenceString(context, key, EMPTY);
        if (data.length() > 0) {
            AccessTokenModel model = gson.fromJson(data, AccessTokenModel.class);
            return model;
        }
        return null;
    }


    /**
     * It is used to save {@link AccessTokenModel} in {@link android.content.SharedPreferences}
     *
     * @param context : {@link Context}
     * @param data
     */
    public void saveAccessTokenModel(Context context, AccessTokenModel data) {
        String key = context.getString(R.string.sharedPredKey_LoginInfo);
        SharedPreferenceHelper.setSharedPreferenceString(context, key, new Gson().toJson(data));
    }


    /**
     * @param context Context to fetch token from {@link android.content.SharedPreferences}.
     * @return true if accesstoken is present.
     */
    public boolean isAuthTokenPresent(Context context) {
        return WebControllerHelper.getAccessTokenPref(context).length() > 0;
    }

    /**
     * @param context : Context of calling class
     * @return : true if accesstoken {from {@link AccessTokenModel} saved in {@link android.content.SharedPreferences}} is not expiring in 10 minutes.
     * <br/> If accesstoken is going to be expired in 10 minutes we are returning false.
     */
    public boolean isAuthTokenValid(Context context) {
        boolean isAccessTokenPresent = isAuthTokenPresent(context);
        if (isAccessTokenPresent) {
            AccessTokenModel accessTokenModel = getAccessTokenModel(context);
            long tokenFetchedTimeInMillis = Long.parseLong(accessTokenModel.getTokenFetchedTimeMilli());
            long timeValidity = Long.parseLong(accessTokenModel.getExpireTimeInSeconds()) * 1000;
            long timeDifference = System.currentTimeMillis() - tokenFetchedTimeInMillis;
            boolean isAccessTokenValid = timeDifference < (timeValidity - 10 * 60 * 1000);
            return isAccessTokenValid;
        }
        return false;
    }

    /**
     * It is used to fetch required parameters from {@link AccountModel} object to make connection.
     * <br/>
     *
     * @param accountModel : {@link AccountModel} instance
     * @return : Json Data required for making connection
     */
    public String getContactJson(AccountModel accountModel) {
        UserInfo.getInstance().setAccountModel(accountModel);
        ModelContactShare modelContactShare = new ModelContactShare(accountModel.getId(), accountModel.getAccountNumber(), accountModel.getSortCode(), accountModel.getName());
        String data = new Gson().toJson(modelContactShare);
        return data;
    }


    /**
     * It converts resource id to {@link RoundedBitmapDrawable} and inflate on ImageView .
     *
     * @param context : Context for Glide
     * @param resId   : resource of image
     * @param view    : ImageView instance
     */
    public void getCircularResourceImage(final Context context, int resId, ImageView view) {
        Glide.with(context)
                .load(resId)
                .asBitmap().centerCrop()
                .into(new BitmapImageViewTarget(view) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            view.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    /**
     * This is a parsing method for time formatted in "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" (eg. "2017-08-02T06:05:30.000Z" ).
     * <br></br>
     * Its role is to parse time to local time.
     *
     * @return Date object
     * @throws ParseException : If parsing is unsuccessful.
     */
    public Date parseTime(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.parse(time);
    }


    /**
     * This method is to get date as {@link String}.
     * <br/>
     * The output will be
     * <br/>
     * dd mmm yyyy hh:mm meridian
     * <br/>
     * <b>merian will be either am or pm depending upon 24 hour time.</b>
     *
     * @param date
     * @return
     */
    public String parseDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        String[] months = {"Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"};


        String monthAlphabetically = months[calendar.get(Calendar.MONTH)];

        String date_ =
                parseIntto2Digitsif1(calendar.get(Calendar.DAY_OF_MONTH)) + TOKEN_SPACE
                        + monthAlphabetically + TOKEN_SPACE
                        + parseIntto2Digitsif1(calendar.get(Calendar.YEAR));

        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int hour = hourOfDay;
        String meridian = "am";

        if (hourOfDay > 12) {
            hour = hourOfDay - 12;
            meridian = "pm";
        }

        String time_ =
                parseIntto2Digitsif1(hour) + TOKEN_DATE_TIME
                        + parseIntto2Digitsif1((calendar.get(Calendar.MINUTE) + 1)) + TOKEN_SPACE
                        + meridian;


        return date_ + TOKEN_SPACE + time_;
    }


    /**
     * It returns string parsing from integer.
     * <p>
     * <br/>
     * <p>
     * If value lies between 0 to 9, it appends 0 as prefix. (eg. for 1 , it will return 01).
     *
     * @param value : integer value
     * @return
     */
    private String parseIntto2Digitsif1(int value) {

        if (value >= 0 && value <= 9) {
            return "0" + value;
        }
        return String.valueOf(value);
    }


    /**
     * Conversion of string to Title case.
     * <br/>
     * eg. we have string "hello I AM GAGAN".
     * <br/>
     * It will convert it to Hello I Am Gagan.
     *
     * @param data : string to be converted
     * @return : converted to TitleCase
     */
    public String convertToTitleCase(String data) {
        return WordUtils.capitalizeFully(data);
    }


    /**
     * It is reusable method to open {@link AlertDialog}.
     *
     * @param context         Context of calling class
     * @param string          Message to show on {@link AlertDialog}
     * @param onClickListener DialogInterface.OnClickListener instance to call when Positive button is clicked by the user.
     */
    public void showDialog(Context context, String string, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(string);
        builder.setPositiveButton(context.getString(R.string.OK), onClickListener);
        builder.setNegativeButton(context.getString(R.string.cancel), null);
        builder.show();
    }


    /**
     * It parse {@link Double} and terminate the decimal if decimal value is zero.
     * <br/>
     * eg. 32.0 returns 32 and 43.1 returns 43.1
     *
     * @param data
     * @return
     */
    public String parseDouble(double data) {
        long doubleParsed = Math.round(data);
        if (doubleParsed == data) {
            return String.valueOf(doubleParsed);
        } else {
            return String.valueOf(data);
        }
    }

}
