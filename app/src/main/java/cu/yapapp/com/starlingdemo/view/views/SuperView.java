package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;
import android.widget.Toast;

import cu.yapapp.com.starlingdemo.R;
import pay.eu.android.model.AccountModel;

/**
 * Created by gaganpreet on 11/9/17.
 */

public abstract class SuperView extends YAView {

    public SuperView(Context context) {
        super(context);
    }

    /**
     * Initialisation of fields should be done in this method.
     */
    public abstract void initValiables();


    /**
     * Setting event callbacks like Touch and click Callbacks of fields should be done in this method.
     */
    public abstract void setEventCallbacks();

    /**
     * This methos is used to show message to user (like {@link Toast}).
     *
     * @param message
     */
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
