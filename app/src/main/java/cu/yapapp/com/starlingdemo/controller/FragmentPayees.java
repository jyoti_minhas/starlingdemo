package cu.yapapp.com.starlingdemo.controller;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cu.yapapp.com.starlingdemo.libraries.barcodereader.BarcodeCaptureActivity;
import com.google.gson.Gson;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.PayeesServiceManager;
import cu.yapapp.com.starlingdemo.model.dataModel.ModelContactShare;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.utils.UserInfo;
import cu.yapapp.com.starlingdemo.view.views.PayeesView;
import pay.eu.android.model.ContactModel;
import pay.eu.android.model.PayeeModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPayees extends FragmentAppbar  {

    private PayeesView mPayeesView = null;
    private PayeesServiceManager payeesServiceManager;
    private StarlingWebController mStarlingWebController;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mPayeesView != null) return mPayeesView;
        mPayeesView = new PayeesView(getContext(), payeeAdapterCallback);
        mStarlingWebController = new StarlingWebController(getActivity());
        payeesServiceManager = new PayeesServiceManager(mStarlingWebController, managerCallbacks);
        UserInfo.getInstance().setBarcodeDetectedListener(barcodeDetectedListener);
        payeesServiceManager.requestPayeesList();
        return mPayeesView;
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitleTxt(getString(R.string.beneficiaries));
    }




    UserInfo.BarcodeDetectedListener barcodeDetectedListener = new UserInfo.BarcodeDetectedListener() {
        @Override
        public void onBarcodeDetected(String data) {
            ModelContactShare modelContactShare = null;
            try {
                modelContactShare = new Gson().fromJson(data, ModelContactShare.class);
            } catch (Exception | Error e) {
                e.printStackTrace();
            }
            if (modelContactShare != null) {
                payeesServiceManager.addPayee(modelContactShare);
            }else {
                mPayeesView.showMessage(getString(R.string.invalid_qrcode));
            }
        }
    };





    PayeesServiceManager.PayeesServiceManagerCallbacks managerCallbacks = new PayeesServiceManager.PayeesServiceManagerCallbacks() {
        @Override
        public void onPayeesReceived(PayeeModel payeeModel, String body) {
            UserInfo.getInstance().setPayees(payeeModel);
            mPayeesView.inflateList(payeeModel.getEmbeddedData().getContacts());
        }

        @Override
        public void onError(String body) {
            /**
             * No code for now as Starling sdk needs to be updated.
             */
        }

        @Override
        public void onPayeeAdded(String id) {
            Toast.makeText(getContext(), getString(R.string.success_add_payee) + " " + id, Toast.LENGTH_LONG).show();
            payeesServiceManager.requestPayeesList();
        }

    };


    PayeesView.PayeeViewListener payeeAdapterCallback = new PayeesView.PayeeViewListener() {

        @Override
        public void addPayee() {
            Intent intent = new Intent(getContext(), BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
            intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
            startActivityForResult(intent, 1001);
        }

        @Override
        public void open(ContactModel contactModel) {
            FragmentPay fragmentPay = new FragmentPay();
            fragmentPay.setContact(contactModel);
            replaceFragment(fragmentPay, true, true);
        }

    };
}
