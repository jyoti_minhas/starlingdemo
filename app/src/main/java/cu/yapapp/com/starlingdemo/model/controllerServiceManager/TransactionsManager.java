package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import pay.eu.android.model.TransactionHistoryModel;
import retrofit2.Response;

/**
 * Created by gaganpreet on 15/9/17.
 */

public class TransactionsManager {


    private StarlingWebController starlingWebController;
    private TransactionManagerCallbacks transactionManagerCallbacks;


    public TransactionsManager(StarlingWebController starlingWebController, TransactionManagerCallbacks transactionManagerCallbacks) {
        this.starlingWebController = starlingWebController;
        this.transactionManagerCallbacks = transactionManagerCallbacks;
    }

    public void requestTransactions() {
        starlingWebController.getPaymentHistoryWebCall(callback, true);
    }


    WebHandler.OnWebCallback callback = new WebHandler.OnWebCallback() {
        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
            TransactionHistoryModel transactionHistoryModel = (TransactionHistoryModel) t;
            transactionManagerCallbacks.onTransactionsReceived(transactionHistoryModel, (String) response.body());
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            transactionManagerCallbacks.onError((String) response.body());
        }
    };


    public interface TransactionManagerCallbacks {

        /**
         * This is a callback method to send transactionHistoryModel to controller
         *
         * @param payeeModel {@link TransactionHistoryModel} instance
         * @param body       String response from the server
         */
        public void onTransactionsReceived(TransactionHistoryModel payeeModel, String body);

        /**
         * This is used to send error message to the controller when transaction is not received.
         *
         * @param body String body received from the server.
         */
        public void onError(String body);
    }


}
