package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;
import cu.yapapp.com.starlingdemo.view.adapters.PayeesAdapter;
import pay.eu.android.model.ContactModel;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class PayeesView extends SuperView {


    private RecyclerView rvPayeesView;
    private PayeeViewListener payeeAdapterCallback;
    private ImageView ivAddPayees;


    public PayeesView(Context context, PayeeViewListener payeeAdapterCallback) {
        super(context);
        this.payeeAdapterCallback = payeeAdapterCallback;
        initValiables();
        setEventCallbacks();
    }


    @Override
    public void initValiables() {
        inflate(getContext(), R.layout.fragments_payees, this);
        rvPayeesView = (RecyclerView) findViewById(R.id.rvPayees);
        ivAddPayees = (ImageView) findViewById(R.id.ivAddPayees);
        rvPayeesView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    @Override
    public void setEventCallbacks() {
        ivAddPayees.setOnClickListener(addPayeeListener);
    }



    OnClickListener addPayeeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            payeeAdapterCallback.addPayee();
        }
    };


    /**
     * It inflate List of contacts in adapter
     * @param contacts
     */
    public void inflateList(List<ContactModel> contacts) {
        rvPayeesView.setAdapter(new PayeesAdapter(getContext() , contacts, payeeAdapterCallback));
    }

    public interface PayeeViewListener extends PayeesAdapter.PayeeAdapterCallback {
        /**
         * Method is called when adding a payee.
         */
        public void addPayee();
    }

}

