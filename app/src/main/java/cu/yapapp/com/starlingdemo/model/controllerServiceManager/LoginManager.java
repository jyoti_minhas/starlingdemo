package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.base.http.WebHandler;
import android.base.util.ApplicationUtils;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;

import cu.yapapp.com.starlingdemo.view.views.LoginView;
import pay.eu.android.Config;
import pay.eu.android.http.pg.WebConstant;
import pay.eu.android.http.pg.WebController;
import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AccountModel;
import retrofit2.Response;

/**
 * Created by gaganpreet on 11/9/17.
 */

public class LoginManager {

    private LoginManagerCallback loginManagerCallback;
    private WebController webController;


    public LoginManager(  WebController webController, LoginManagerCallback loginManagerCallback) {

        this.webController = webController;
        this.loginManagerCallback = loginManagerCallback;
    }

    public LoginView.LoginViewCallbacks getLoginCallback() {
        return loginViewCallbacks;
    }


    private LoginView.LoginViewCallbacks loginViewCallbacks = new LoginView.LoginViewCallbacks() {

        public boolean handleOverrideUrl(final Uri uri) {
            ApplicationUtils.Log.i(getTag(), "Uri =" + uri);
            final String host = uri.getHost();
            final String scheme = uri.getScheme();
            final String queryParameterCode = uri.getQueryParameter(WebConstant.CODE_PARAM);
            if (uri.toString().startsWith(Config.getBaseWebUrlOauthCallback())
                    && !ApplicationUtils.Validator.isEmptyOrNull(queryParameterCode)) {
                webController.getAccessTokenWebCall(uri, onWebCallback, true);
                return true;
            } else {
                try {
                    if (scheme.endsWith("mp3")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        loginManagerCallback.openIntent(intent);
                        return false;
                    } else if (scheme.startsWith("http")
                            || scheme.startsWith("https")) {
                        return false;
                    }
                    // Otherwise allow the OS to handle it
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    loginManagerCallback.openIntent(intent);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }
    };

    private String getTag() {
        return getClass().getCanonicalName();
    }


    private WebHandler.OnWebCallback onWebCallback = new WebHandler.OnWebCallback() {
        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
            AccessTokenModel accessTokenModel = (AccessTokenModel) t;
            if (accessTokenModel != null && accessTokenModel.getAccessToken().length() > 0) {
                loginManagerCallback.onAccessTokenReceived(accessTokenModel, (String) response.body());
            } else
                loginManagerCallback.onErrorWhileLogin((String) response.body());
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            loginManagerCallback.onErrorWhileLogin((String) response.body());
        }
    };

    public interface LoginManagerCallback {

        /**
         * It sends callback to controller to start component.
         *
         * @param intent
         */
        public void openIntent(Intent intent);

        /**
         * This sends {@link AccessTokenModel} to the controlled when received from api.
         *
         * @param accessTokenModel
         * @param body
         */
        public void onAccessTokenReceived(AccessTokenModel accessTokenModel, String body);

        /**
         * This sends error to controller
         *
         * @param body
         */
        public void onErrorWhileLogin(String body);
    }


}
