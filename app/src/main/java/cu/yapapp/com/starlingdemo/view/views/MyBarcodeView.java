package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;

import com.bottlerocketstudios.barcode.generation.ui.BarcodeView;

import cu.yapapp.com.starlingdemo.R;

/**
 * Created by gaganpreet on 13/9/17.
 */

public class MyBarcodeView extends SuperView {


    private BarcodeView generation_barcode_image;

    public MyBarcodeView(Context context) {
        super(context);

        initValiables();

        setEventCallbacks();

    }

    @Override
    public void initValiables() {
        inflate(getContext(), R.layout.fragment_my_barcode, this);

        generation_barcode_image = (BarcodeView) findViewById(R.id.generation_barcode_image);
    }

    @Override
    public void setEventCallbacks() {
        /**
         * No view to capture events for now.
         */
    }


    /**
     * It loads barcode of given @param text
     * @param text
     */
    public void setText(String text) {
        generation_barcode_image.setBarcodeText(text);
    }


}
