package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


/**
 * Created by yapapp on 24/11/16.
 */
public class YAView extends RelativeLayout {
    ImageView imageEmptyView;

    public interface YAViewInterface{



    }


    private Context context;
    private YAViewInterface yaViewInterface;

    public YAView(Context context) {
        super(context);
        this.context=context;
        imageEmptyView=new ImageView(context);
        setClickable(true);
    }



    public YAView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        imageEmptyView=new ImageView(context);
        setClickable(true);
    }

    public YAView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        imageEmptyView=new ImageView(context);
        setClickable(true);
    }

    public YAView(Context context, YAViewInterface yaViewInterface){
        super(context);
        this.context=context;
        this.yaViewInterface=yaViewInterface;
        imageEmptyView=new ImageView(context);
        setClickable(true);
    }


    public void showEmptyView(int resourceFile){
        imageEmptyView.setImageResource(resourceFile);


        LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams)imageEmptyView.getLayoutParams();
        ll.gravity = Gravity.CENTER;
        imageEmptyView.setLayoutParams(ll);

        this.addView(imageEmptyView);

    }


    public void hideEmptyView(){

       int index=-1;

        for(int i=0;i<getChildCount();i++){

            View view=getChildAt(i);
            if(view==imageEmptyView){
                index=i;
            }
        }

        if(index>=0){
            removeViewAt(index);
        }
    }

}
