package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import cu.yapapp.com.starlingdemo.model.dataModel.ModelContactShare;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import pay.eu.android.http.pg.WebConstant;
import pay.eu.android.model.PayeeDetailResponseModel;
import pay.eu.android.model.PayeeModel;
import pay.eu.android.module.AccountType;
import retrofit2.Response;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class PayeesServiceManager {

    private StarlingWebController mStarlingWebController;
    private PayeesServiceManagerCallbacks payeesServiceManagerCallbacks;


    /**
     * Initialisation of objects.
     */
    public PayeesServiceManager(StarlingWebController starlingWebController, PayeesServiceManagerCallbacks payeesServiceManagerCallbacks) {
        this.mStarlingWebController = starlingWebController;
        this.payeesServiceManagerCallbacks = payeesServiceManagerCallbacks;
    }


    /**
     *  Used to request payeeList
     */
    public void requestPayeesList() {
        mStarlingWebController.getPayeesWebCall(getPayeesWebCall, true);
    }


    /**
     *  For adding a benificiary we need to get detail of it.
     * @param userId UserId to get detail of benificiary
     * @param payeeDetail details of benificiary
     */
    private void getPayeeDetail(String userId, final PayeeDetail payeeDetail) {
        mStarlingWebController.getPayeeDetailWebCall(userId, new WebHandler.OnWebCallback() {
            @Override
            public <T> void onSuccess(@Nullable T t, int i, Response response) {
                payeeDetail.onPayeeReturned((PayeeDetailResponseModel) t);
            }

            @Override
            public <T> void onError(@Nullable T t, String s, int i, Response response) {
                payeesServiceManagerCallbacks.onError((String) response.body());
            }
        }, true);
    }


    /**
     * This should be called when calling a payee.
     *
     * @param payeeDetailResponseModel {@link ModelContactShare}
     */
    public void addPayee(final ModelContactShare payeeDetailResponseModel) {
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(WebConstant.ACCOUNT_NUMBER_PARAM, payeeDetailResponseModel.getAccount());
        params.put(WebConstant.ID_PARAM, payeeDetailResponseModel.getId());
        params.put(WebConstant.SORT_CODE_PARAM, payeeDetailResponseModel.getSortCode());
        params.put(WebConstant.TYPE_PARAM, AccountType.UK_ACCOUNT_AND_SORT_CODE.name());
        params.put(WebConstant.NAME_PARAM, payeeDetailResponseModel.getName());
        mStarlingWebController.addPayeeWebCall(params, new WebHandler.OnWebCallback() {
            @Override
            public <T> void onSuccess(@Nullable T t, int i, Response response) {
                payeesServiceManagerCallbacks.onPayeeAdded(payeeDetailResponseModel.getId());
            }

            @Override
            public <T> void onError(@Nullable T t, String s, int i, Response response) {
                payeesServiceManagerCallbacks.onError((String) response.body());
            }
        }, true);
    }


    WebHandler.OnWebCallback getPayeesWebCall = new WebHandler.OnWebCallback() {
        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
            PayeeModel payeeModel = (PayeeModel) t;
            payeesServiceManagerCallbacks.onPayeesReceived(payeeModel, (String) response.body());
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            payeesServiceManagerCallbacks.onError((String) response.body());
        }
    };


    public interface PayeeDetail {
        /**
         * callback method to intercept when payee detail is returned.
         * @param payeeDetailResponseModel
         */
        public void onPayeeReturned(PayeeDetailResponseModel payeeDetailResponseModel);
    }


    public interface PayeesServiceManagerCallbacks {

        /**
         *  Callback to controller is passed when payees list is received.
         */
        public void onPayeesReceived(PayeeModel payeeModel, String body);


        /**
         *  Callback to controller is passed when error incurred while consuming Api.
         */
        public void onError(String body);


        /**
         *  Callback to controller is passed when payee is added.
         */
        public void onPayeeAdded(String id);

    }

}
