package cu.yapapp.com.starlingdemo.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cu.yapapp.com.starlingdemo.model.controllerServiceManager.SplashManager;
import cu.yapapp.com.starlingdemo.view.views.SplashView;

/**
 * Created by gaganpreet on 11/9/17.
 */

public class SplashFragment extends YABaseFragment {

    private SplashView splashView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        splashView = new SplashView(getContext());
        SplashManager splashManager = new SplashManager(new SplashManager.SplashManagerCallback() {
            @Override
            public void openDashboard() {
                popSingleFragment();
                replaceFragment(new LoginFragment(), true, true);
            }
        });
        splashManager.startCounting();
        return splashView;
    }

}
